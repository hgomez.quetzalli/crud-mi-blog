#Mi blog

## Pasos para Crear y Ejecutar la Base de Datos con Docker

    docker build -t mi_blog_db .
    docker run -p 5433:5432 -d mi_blog_db

   Asegúrate de seguir estos pasos para crear y ejecutar tu base de datos utilizando Docker. Los comandos anteriores construirán la imagen con el nombre `mi_blog_db` y ejecutarán el contenedor, exponiendo el puerto 5433 en tu máquina local.

## Pasos para Abrir la Base de Datos en pgAdmin

1. Abrir pgAdmin.
2. Hacer clic derecho en "Servers".
3. Seleccionar "Register".
4. Presionar "Server".
5. En la sección "General", asignar un nombre al servidor.
6. En la sección "Connection":
   - Hostname/Address: localhost
   - Port: 5433
   - Maintenance Database: mi_blog_db
   - Username: usuarioCiencias
   - Password: Caract3r.

## Ejecutar Proyecto NestJS sin Dockerfile

Para ejecutar el proyecto NestJS sin utilizar Dockerfile, sigue los siguientes pasos:

1. Abre el archivo `.env`.
   - Modifica la variable `POSTGRES_PORT` y establece su valor en `5433`.
   - Modifica la variable `POSTGRES_HOST` y estabece su valor en `localhost`

2. Navega a la carpeta `p1/src/my_blog`.

3. Ejecuta el siguiente comando:
   ```bash
   npm run start:dev
   ```
## Ejecutar Proyecto NestJS con Dockerfile

1. En el archivo `.env`, establecer el valor de la variable `POSTGRES_PORT` en 5432.

2. Obtener la dirección IP del contenedor de la base de datos con el siguiente comando:

    ```bash
    docker inspect nombre_del_contenedor
    ```

3. Colocar la dirección IP obtenida dentro de la variable `POSTGRES_HOST` en el archivo `.env`.
