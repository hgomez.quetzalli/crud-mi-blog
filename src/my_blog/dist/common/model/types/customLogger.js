"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogType = exports.LogServices = void 0;
var LogServices;
(function (LogServices) {
    LogServices["Postgres"] = "Postgres";
})(LogServices || (exports.LogServices = LogServices = {}));
var LogType;
(function (LogType) {
    LogType["Request"] = "REQUEST";
    LogType["Response"] = "RESPONSE";
    LogType["Payload"] = "PAYLOAD";
    LogType["Error"] = "ERROR";
})(LogType || (exports.LogType = LogType = {}));
//# sourceMappingURL=customLogger.js.map