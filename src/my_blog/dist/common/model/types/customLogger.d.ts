export declare enum LogServices {
    Postgres = "Postgres"
}
export declare enum LogType {
    Request = "REQUEST",
    Response = "RESPONSE",
    Payload = "PAYLOAD",
    Error = "ERROR"
}
