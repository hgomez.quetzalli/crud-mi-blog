"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnlacesController = void 0;
const common_1 = require("@nestjs/common");
const create_enlace_dto_1 = require("../dto/create-enlace.dto");
const update_enlace_dto_1 = require("../dto/update-enlace.dto");
const enlaces_business_impl_1 = require("../business/impl/enlaces.business.impl");
const passport_1 = require("@nestjs/passport");
let EnlacesController = class EnlacesController {
    constructor(enlacesBusiness) {
        this.enlacesBusiness = enlacesBusiness;
    }
    create(createEnlaceDto) {
        return this.enlacesBusiness.create(createEnlaceDto);
    }
    findAll() {
        return this.enlacesBusiness.findAll();
    }
    findOne(id) {
        return this.enlacesBusiness.findOne(+id);
    }
    update(id, updateEnlaceDto) {
        return this.enlacesBusiness.update(+id, updateEnlaceDto);
    }
    remove(id) {
        return this.enlacesBusiness.remove(+id);
    }
};
exports.EnlacesController = EnlacesController;
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_enlace_dto_1.CreateEnlaceDto]),
    __metadata("design:returntype", void 0)
], EnlacesController.prototype, "create", null);
__decorate([
    (0, common_1.Get)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], EnlacesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EnlacesController.prototype, "findOne", null);
__decorate([
    (0, common_1.Patch)(':id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, update_enlace_dto_1.UpdateEnlaceDto]),
    __metadata("design:returntype", void 0)
], EnlacesController.prototype, "update", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], EnlacesController.prototype, "remove", null);
exports.EnlacesController = EnlacesController = __decorate([
    (0, common_1.Controller)('enlaces'),
    __metadata("design:paramtypes", [enlaces_business_impl_1.EnlaceBusiness])
], EnlacesController);
//# sourceMappingURL=enlaces.controller.js.map