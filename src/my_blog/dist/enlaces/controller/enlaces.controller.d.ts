import { CreateEnlaceDto } from '../dto/create-enlace.dto';
import { UpdateEnlaceDto } from '../dto/update-enlace.dto';
import { EnlaceBusiness } from '../business/impl/enlaces.business.impl';
export declare class EnlacesController {
    private readonly enlacesBusiness;
    constructor(enlacesBusiness: EnlaceBusiness);
    create(createEnlaceDto: CreateEnlaceDto): Promise<import("../entities/enlace.entity").Enlace>;
    findAll(): Promise<import("../entities/enlace.entity").Enlace[]>;
    findOne(id: string): Promise<import("../entities/enlace.entity").Enlace>;
    update(id: string, updateEnlaceDto: UpdateEnlaceDto): Promise<import("../entities/enlace.entity").Enlace>;
    remove(id: string): Promise<{
        affected?: number;
    }>;
}
