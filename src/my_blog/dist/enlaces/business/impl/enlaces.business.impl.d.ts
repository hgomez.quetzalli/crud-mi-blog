import { IEnlaceBusiness } from "../enlaces.business";
import { IEnlaceService } from "src/enlaces/services/enlaces.service";
import { CreateEnlaceDto } from "src/enlaces/dto/create-enlace.dto";
import { Enlace } from "src/enlaces/entities/enlace.entity";
import { UpdateEnlaceDto } from "src/enlaces/dto/update-enlace.dto";
export declare class EnlaceBusiness implements IEnlaceBusiness {
    private readonly enlacesService;
    constructor(enlacesService: IEnlaceService);
    create(createEnlaceDto: CreateEnlaceDto): Promise<Enlace>;
    findAll(): Promise<Enlace[]>;
    findOne(id: number): Promise<Enlace>;
    update(id: number, updatePublicacioneDto: UpdateEnlaceDto): Promise<Enlace>;
    remove(id: number): Promise<{
        affected?: number;
    }>;
}
