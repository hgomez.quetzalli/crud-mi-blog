"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnlacesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const enlaces_service_impl_1 = require("./services/impl/enlaces.service.impl");
const enlaces_controller_1 = require("./controller/enlaces.controller");
const enlace_entity_1 = require("./entities/enlace.entity");
const enlaces_business_impl_1 = require("./business/impl/enlaces.business.impl");
let EnlacesModule = class EnlacesModule {
};
exports.EnlacesModule = EnlacesModule;
exports.EnlacesModule = EnlacesModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([enlace_entity_1.Enlace])],
        controllers: [enlaces_controller_1.EnlacesController],
        providers: [enlaces_business_impl_1.EnlaceBusiness, { provide: 'IEnlacesService', useClass: enlaces_service_impl_1.EnlacesService }],
    })
], EnlacesModule);
//# sourceMappingURL=enlaces.module.js.map