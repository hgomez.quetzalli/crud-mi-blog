import { Repository } from 'typeorm';
import { CreateEnlaceDto } from '../../dto/create-enlace.dto';
import { UpdateEnlaceDto } from '../../dto/update-enlace.dto';
import { Enlace } from 'src/enlaces/entities/enlace.entity';
import { IEnlaceService } from '../enlaces.service';
export declare class EnlacesService implements IEnlaceService {
    private readonly enlaceRepository;
    constructor(enlaceRepository: Repository<Enlace>);
    create(createEnlaceDto: CreateEnlaceDto): Promise<Enlace>;
    findAll(): Promise<Enlace[]>;
    findOne(id: number): Promise<Enlace>;
    update(id: number, updateEnlaceDto: UpdateEnlaceDto): Promise<Enlace>;
    remove(id: number): Promise<{
        affected?: number;
    }>;
}
