"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const config_1 = require("@nestjs/config");
const dotenv_1 = require("dotenv");
const publicaciones_module_1 = require("./publicaciones/publicaciones.module");
const publicacion_entity_1 = require("./publicaciones/entities/publicacion.entity");
const imagen_entity_1 = require("./publicaciones/entities/imagen.entity");
const publicacion_imagen_entity_1 = require("./publicaciones/entities/publicacion-imagen.entity");
const categorias_entity_1 = require("./publicaciones/entities/categorias.entity");
const publicaciones_categorias_entity_1 = require("./publicaciones/entities/publicaciones-categorias.entity");
const enlaces_module_1 = require("./enlaces/enlaces.module");
const enlace_entity_1 = require("./enlaces/entities/enlace.entity");
const auth_module_1 = require("./auth/auth.module");
(0, dotenv_1.config)();
const configService = new config_1.ConfigService();
let AppModule = class AppModule {
};
exports.AppModule = AppModule;
exports.AppModule = AppModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forRoot({
                type: 'postgres',
                host: configService.get('POSTGRES_HOST'),
                port: configService.get('POSTGRES_PORT'),
                username: configService.get('POSTGRES_USER'),
                password: configService.get('POSTGRES_PASSWORD'),
                database: configService.get('POSTGRES_DATABASE'),
                entities: [publicacion_entity_1.publicaciones, imagen_entity_1.imagenes, publicacion_imagen_entity_1.publicaciones_imagenes, categorias_entity_1.categorias, publicaciones_categorias_entity_1.publicaciones_categorias, enlace_entity_1.Enlace],
                synchronize: true,
            }),
            publicaciones_module_1.PublicacionesModule,
            enlaces_module_1.EnlacesModule,
            auth_module_1.AuthModule
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
//# sourceMappingURL=app.module.js.map