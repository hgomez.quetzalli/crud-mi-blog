"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomLogger = void 0;
const common_1 = require("@nestjs/common");
const lodash_1 = require("lodash");
const customLogger_1 = require("../common/model/types/customLogger");
const uuid_1 = require("uuid");
let CustomLogger = class CustomLogger extends common_1.ConsoleLogger {
    constructor() {
        super();
        this.trackingId = (0, uuid_1.v4)();
    }
    buildLog(serviceName, options) {
        const { id: customId, type = customLogger_1.LogType.Payload, message, payload } = options || {};
        const formattedId = customId ? ` (${customId})` : '';
        const fornattedMessage = message ? ` ${message}` : '';
        const formattedPayload = (0, lodash_1.isEmpty)(payload) ? '' : `: ${JSON.stringify(payload)}`;
        return [`[${serviceName}] [${this.trackingId}] [${type}]`, formattedId, fornattedMessage, formattedPayload].join('');
    }
    serviceLog(serviceName, options) {
        super.log(this.buildLog(serviceName, options));
    }
    serviceError(serviceName, options) {
        super.error(this.buildLog(serviceName, options));
    }
};
exports.CustomLogger = CustomLogger;
exports.CustomLogger = CustomLogger = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.REQUEST }),
    __metadata("design:paramtypes", [])
], CustomLogger);
//# sourceMappingURL=CustomLogger.js.map