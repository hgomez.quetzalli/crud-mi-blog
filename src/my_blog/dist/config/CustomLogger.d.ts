import { ConsoleLogger } from "@nestjs/common";
import { LogServices, LogType } from "src/common/model/types/customLogger";
type LogOptions = {
    id?: string | number;
    type?: LogType;
    message?: string;
    payload?: any;
};
export declare class CustomLogger extends ConsoleLogger {
    readonly trackingId: string;
    constructor();
    private buildLog;
    serviceLog(serviceName: LogServices, options?: LogOptions): void;
    serviceError(serviceName: LogServices, options?: LogOptions): void;
}
export {};
