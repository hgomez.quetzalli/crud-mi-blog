"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicacionesModule = void 0;
const common_1 = require("@nestjs/common");
const publicaciones_service_impl_1 = require("./services/impl/publicaciones.service.impl");
const publicaciones_controller_1 = require("./controller/publicaciones.controller");
const typeorm_1 = require("@nestjs/typeorm");
const publicacion_entity_1 = require("./entities/publicacion.entity");
const imagen_entity_1 = require("./entities/imagen.entity");
const publicaciones_business_impl_1 = require("./business/impl/publicaciones.business.impl");
const publicacion_imagen_entity_1 = require("./entities/publicacion-imagen.entity");
const categorias_entity_1 = require("./entities/categorias.entity");
const publicaciones_categorias_entity_1 = require("./entities/publicaciones-categorias.entity");
let PublicacionesModule = class PublicacionesModule {
};
exports.PublicacionesModule = PublicacionesModule;
exports.PublicacionesModule = PublicacionesModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([publicacion_entity_1.publicaciones, imagen_entity_1.imagenes, publicacion_imagen_entity_1.publicaciones_imagenes, categorias_entity_1.categorias, publicaciones_categorias_entity_1.publicaciones_categorias])],
        controllers: [publicaciones_controller_1.PublicacionesController],
        providers: [publicaciones_business_impl_1.PublicacionesBusiness, { provide: 'IPublicacionesService', useClass: publicaciones_service_impl_1.PublicacionesService }],
    })
], PublicacionesModule);
//# sourceMappingURL=publicaciones.module.js.map