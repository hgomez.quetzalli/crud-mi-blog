import { CreaPublicacionesDto } from "../dto/crea-publicacion.dto";
import { CreaImagenDto } from "../dto/crea-imagen.dto";
import { CreaPublicacionImagenDto } from "../dto/crea-publicacion-imagen.dto";
import { publicaciones } from "../entities/publicacion.entity";
import { imagenes } from "../entities/imagen.entity";
import { publicaciones_imagenes } from "../entities/publicacion-imagen.entity";
import { CreaCategoriaDto } from "../dto/crea-categoria.dto";
import { categorias } from "../entities/categorias.entity";
import { CreaPublicacionCategoriaDto } from "../dto/crea-publicacion-categoria.dto";
import { publicaciones_categorias } from "../entities/publicaciones-categorias.entity";
export interface IPublicacionesService {
    create(creaPublicacionesDto: CreaPublicacionesDto): Promise<publicaciones>;
    createImagen(creaImagenDto: CreaImagenDto): Promise<imagenes>;
    createPublicacionImagen(creaPublicacionImagenDto: CreaPublicacionImagenDto): Promise<publicaciones_imagenes>;
    createCategoria(creaCategoriaDto: CreaCategoriaDto): Promise<categorias>;
    createPublicacionCategoria(creaPublicacionCategoriaDto: CreaPublicacionCategoriaDto): Promise<publicaciones_categorias>;
    findAll(): Promise<publicaciones[]>;
    findAllImagenes(): Promise<imagenes[]>;
    findAllPublicacionImagenes(): Promise<publicaciones_imagenes[]>;
    findAllCategorias(): Promise<categorias[]>;
    findAllPublicacionCategorias(): Promise<publicaciones_categorias[]>;
    findOne(id: number): Promise<publicaciones>;
    findOneImagen(id: number): Promise<imagenes>;
    findOnePublicacionImagen(id: number): Promise<publicaciones_imagenes>;
    findOneCategoria(id: number): Promise<categorias>;
    findOnePublicacionCategoria(id: number): Promise<publicaciones_categorias>;
    remove(id: number): Promise<{
        affected?: number;
    }>;
    removeImagen(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionImagen(id: number): Promise<{
        affected?: number;
    }>;
    removeCategorias(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionCategorias(id: number): Promise<{
        affected?: number;
    }>;
}
