"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicacionesService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const publicacion_entity_1 = require("../../entities/publicacion.entity");
const imagen_entity_1 = require("../../entities/imagen.entity");
const publicacion_imagen_entity_1 = require("../../entities/publicacion-imagen.entity");
const categorias_entity_1 = require("../../entities/categorias.entity");
const publicaciones_categorias_entity_1 = require("../../entities/publicaciones-categorias.entity");
let PublicacionesService = class PublicacionesService {
    constructor(publicacionesRepository, imagenesRepository, publicacionesImagenesRepository, categoriasRepository, publicacionesCategoriasRepository) {
        this.publicacionesRepository = publicacionesRepository;
        this.imagenesRepository = imagenesRepository;
        this.publicacionesImagenesRepository = publicacionesImagenesRepository;
        this.categoriasRepository = categoriasRepository;
        this.publicacionesCategoriasRepository = publicacionesCategoriasRepository;
    }
    create(creaPublicacionesDto) {
        const publica = new publicacion_entity_1.publicaciones();
        publica.titulo = creaPublicacionesDto.titulo;
        publica.subtitulo = creaPublicacionesDto.subtitulo;
        publica.contenido = creaPublicacionesDto.contenido;
        publica.fecha_creacion = creaPublicacionesDto.fecha_creacion;
        return this.publicacionesRepository.save(publica);
    }
    createImagen(creaImagenDto) {
        const ima = new imagen_entity_1.imagenes();
        ima.url = creaImagenDto.url;
        ima.descripcion = creaImagenDto.descripcion;
        ima.fecha_subida = creaImagenDto.fecha_subida;
        return this.imagenesRepository.save(ima);
    }
    async createPublicacionImagen(creaPublicacionImagenDto) {
        const nuevaPublicacionImagen = new publicacion_imagen_entity_1.publicaciones_imagenes();
        const idImagen = creaPublicacionImagenDto.imagen_id;
        nuevaPublicacionImagen.imagen = await this.imagenesRepository.findOneBy({ id: idImagen });
        const idPub = creaPublicacionImagenDto.publicacion_id;
        nuevaPublicacionImagen.publicacion = await this.publicacionesRepository.findOneBy({ id: idPub });
        return this.publicacionesImagenesRepository.save(nuevaPublicacionImagen);
    }
    createCategoria(creaCategoriaDto) {
        const cat = new categorias_entity_1.categorias();
        cat.nombre = creaCategoriaDto.nombre;
        return this.categoriasRepository.save(cat);
    }
    async createPublicacionCategoria(creaPublicacionCategoriaDto) {
        const nuevaPublicacionCategoria = new publicaciones_categorias_entity_1.publicaciones_categorias();
        const idImagen = creaPublicacionCategoriaDto.categoria_id;
        nuevaPublicacionCategoria.categoria = await this.categoriasRepository.findOneBy({ id: idImagen });
        const idPub = creaPublicacionCategoriaDto.publicacion_id;
        nuevaPublicacionCategoria.publicacion = await this.publicacionesRepository.findOneBy({ id: idPub });
        return this.publicacionesCategoriasRepository.save(nuevaPublicacionCategoria);
    }
    findAll() {
        return this.publicacionesRepository.find();
    }
    findAllImagenes() {
        return this.imagenesRepository.find();
    }
    findAllPublicacionImagenes() {
        return this.publicacionesImagenesRepository.find({ relations: ['imagen', 'publicacion'] });
    }
    findAllCategorias() {
        return this.categoriasRepository.find();
    }
    findAllPublicacionCategorias() {
        return this.publicacionesCategoriasRepository.find({ relations: ['categoria', 'publicacion'] });
    }
    findOne(id) {
        return this.publicacionesRepository.findOneBy({ id });
    }
    findOneImagen(id) {
        return this.imagenesRepository.findOneBy({ id });
    }
    async findOnePublicacionImagen(id) {
        const publicacionImagen = await this.publicacionesImagenesRepository.findOne({
            where: { id },
            relations: ['imagen', 'publicacion'],
        });
        return publicacionImagen;
    }
    findOneCategoria(id) {
        return this.categoriasRepository.findOneBy({ id });
    }
    async findOnePublicacionCategoria(id) {
        const publicacionCategoria = await this.publicacionesCategoriasRepository.findOne({
            where: { id },
            relations: ['categoria', 'publicacion'],
        });
        return publicacionCategoria;
    }
    remove(id) {
        return this.publicacionesRepository.delete(id);
    }
    removeImagen(id) {
        return this.imagenesRepository.delete(id);
    }
    removePublicacionImagen(id) {
        return this.publicacionesImagenesRepository.delete(id);
    }
    removeCategorias(id) {
        return this.categoriasRepository.delete(id);
    }
    removePublicacionCategorias(id) {
        return this.publicacionesCategoriasRepository.delete(id);
    }
};
exports.PublicacionesService = PublicacionesService;
exports.PublicacionesService = PublicacionesService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(publicacion_entity_1.publicaciones)),
    __param(1, (0, typeorm_1.InjectRepository)(imagen_entity_1.imagenes)),
    __param(2, (0, typeorm_1.InjectRepository)(publicacion_imagen_entity_1.publicaciones_imagenes)),
    __param(3, (0, typeorm_1.InjectRepository)(categorias_entity_1.categorias)),
    __param(4, (0, typeorm_1.InjectRepository)(publicaciones_categorias_entity_1.publicaciones_categorias)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], PublicacionesService);
//# sourceMappingURL=publicaciones.service.impl.js.map