import { Repository } from 'typeorm';
import { IPublicacionesService } from '../publicaciones.service';
import { publicaciones } from '../../entities/publicacion.entity';
import { imagenes } from 'src/publicaciones/entities/imagen.entity';
import { publicaciones_imagenes } from 'src/publicaciones/entities/publicacion-imagen.entity';
import { CreaImagenDto } from 'src/publicaciones/dto/crea-imagen.dto';
import { CreaPublicacionImagenDto } from 'src/publicaciones/dto/crea-publicacion-imagen.dto';
import { CreaPublicacionesDto } from '../../dto/crea-publicacion.dto';
import { CreaCategoriaDto } from 'src/publicaciones/dto/crea-categoria.dto';
import { categorias } from 'src/publicaciones/entities/categorias.entity';
import { CreaPublicacionCategoriaDto } from 'src/publicaciones/dto/crea-publicacion-categoria.dto';
import { publicaciones_categorias } from 'src/publicaciones/entities/publicaciones-categorias.entity';
export declare class PublicacionesService implements IPublicacionesService {
    private readonly publicacionesRepository;
    private readonly imagenesRepository;
    private readonly publicacionesImagenesRepository;
    private readonly categoriasRepository;
    private readonly publicacionesCategoriasRepository;
    constructor(publicacionesRepository: Repository<publicaciones>, imagenesRepository: Repository<imagenes>, publicacionesImagenesRepository: Repository<publicaciones_imagenes>, categoriasRepository: Repository<categorias>, publicacionesCategoriasRepository: Repository<publicaciones_categorias>);
    create(creaPublicacionesDto: CreaPublicacionesDto): Promise<publicaciones>;
    createImagen(creaImagenDto: CreaImagenDto): Promise<imagenes>;
    createPublicacionImagen(creaPublicacionImagenDto: CreaPublicacionImagenDto): Promise<publicaciones_imagenes>;
    createCategoria(creaCategoriaDto: CreaCategoriaDto): Promise<categorias>;
    createPublicacionCategoria(creaPublicacionCategoriaDto: CreaPublicacionCategoriaDto): Promise<publicaciones_categorias>;
    findAll(): Promise<publicaciones[]>;
    findAllImagenes(): Promise<imagenes[]>;
    findAllPublicacionImagenes(): Promise<publicaciones_imagenes[]>;
    findAllCategorias(): Promise<categorias[]>;
    findAllPublicacionCategorias(): Promise<publicaciones_categorias[]>;
    findOne(id: number): Promise<publicaciones>;
    findOneImagen(id: number): Promise<imagenes>;
    findOnePublicacionImagen(id: number): Promise<publicaciones_imagenes>;
    findOneCategoria(id: number): Promise<categorias>;
    findOnePublicacionCategoria(id: number): Promise<publicaciones_categorias>;
    remove(id: number): Promise<{
        affected?: number;
    }>;
    removeImagen(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionImagen(id: number): Promise<{
        affected?: number;
    }>;
    removeCategorias(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionCategorias(id: number): Promise<{
        affected?: number;
    }>;
}
