export declare class CreaPublicacionesDto {
    titulo: string;
    subtitulo: string;
    contenido: string;
    fecha_creacion: Date;
}
