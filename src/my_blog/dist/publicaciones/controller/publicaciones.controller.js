"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicacionesController = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
const crea_publicacion_dto_1 = require("../dto/crea-publicacion.dto");
const publicaciones_business_impl_1 = require("../business/impl/publicaciones.business.impl");
const crea_imagen_dto_1 = require("../dto/crea-imagen.dto");
const crea_publicacion_imagen_dto_1 = require("../dto/crea-publicacion-imagen.dto");
const crea_categoria_dto_1 = require("../dto/crea-categoria.dto");
const crea_publicacion_categoria_dto_1 = require("../dto/crea-publicacion-categoria.dto");
let PublicacionesController = class PublicacionesController {
    constructor(publicacionesBusiness) {
        this.publicacionesBusiness = publicacionesBusiness;
    }
    create(creaPublicacionesDto) {
        return this.publicacionesBusiness.create(creaPublicacionesDto);
    }
    createImagen(creaImagenDto) {
        return this.publicacionesBusiness.createImagenes(creaImagenDto);
    }
    createPublicacionImagen(creaPublicacionImagenDto) {
        return this.publicacionesBusiness.createPublicacionImagen(creaPublicacionImagenDto);
    }
    createCategorias(creaCategoriasDto) {
        return this.publicacionesBusiness.createCategoria(creaCategoriasDto);
    }
    createPublicacionCategoria(creaPublicacionCategoriasDto) {
        return this.publicacionesBusiness.createPublicacionCategoria(creaPublicacionCategoriasDto);
    }
    findAll() {
        return this.publicacionesBusiness.findAll();
    }
    findAllImagenes() {
        return this.publicacionesBusiness.findAllImagenes();
    }
    findAllPublicacionesImagenes() {
        return this.publicacionesBusiness.findAllPublicacionesImagenes();
    }
    findAllCategorias() {
        return this.publicacionesBusiness.findAllCategorias();
    }
    findAllPublicacionCategoria() {
        return this.publicacionesBusiness.findAllPublicacionCategorias();
    }
    findOne(id) {
        return this.publicacionesBusiness.findOne(+id);
    }
    findOneImagen(id) {
        return this.publicacionesBusiness.findOneImagen(+id);
    }
    findOnePublicacionImagen(id) {
        return this.publicacionesBusiness.findOnePublicacionImagen(+id);
    }
    findOneCategorias(id) {
        return this.publicacionesBusiness.findOneCategoria(+id);
    }
    findOnePublicacionCategoria(id) {
        return this.publicacionesBusiness.findOnePublicacionCategoria(+id);
    }
    remove(id) {
        return this.publicacionesBusiness.remove(+id);
    }
    removeImagen(id) {
        return this.publicacionesBusiness.removeImagen(+id);
    }
    removePublicacionImagen(id) {
        return this.publicacionesBusiness.removePublicacionImagen(+id);
    }
    removeCategorias(id) {
        return this.publicacionesBusiness.removeCategorias(+id);
    }
    removePublicacionCategoria(id) {
        return this.publicacionesBusiness.removePublicacionCategorias(+id);
    }
};
exports.PublicacionesController = PublicacionesController;
__decorate([
    (0, common_1.Post)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crea_publicacion_dto_1.CreaPublicacionesDto]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "create", null);
__decorate([
    (0, common_1.Post)('imagen'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crea_imagen_dto_1.CreaImagenDto]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "createImagen", null);
__decorate([
    (0, common_1.Post)('publicacion-imagen'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crea_publicacion_imagen_dto_1.CreaPublicacionImagenDto]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "createPublicacionImagen", null);
__decorate([
    (0, common_1.Post)('categorias'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crea_categoria_dto_1.CreaCategoriaDto]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "createCategorias", null);
__decorate([
    (0, common_1.Post)('publicacion-categoria'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [crea_publicacion_categoria_dto_1.CreaPublicacionCategoriaDto]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "createPublicacionCategoria", null);
__decorate([
    (0, common_1.Get)(),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findAll", null);
__decorate([
    (0, common_1.Get)('imagen'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findAllImagenes", null);
__decorate([
    (0, common_1.Get)('publicacion-imagen'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findAllPublicacionesImagenes", null);
__decorate([
    (0, common_1.Get)('categorias'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findAllCategorias", null);
__decorate([
    (0, common_1.Get)('publicacion-categoria'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findAllPublicacionCategoria", null);
__decorate([
    (0, common_1.Get)(':id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findOne", null);
__decorate([
    (0, common_1.Get)('imagen/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findOneImagen", null);
__decorate([
    (0, common_1.Get)('publicacion-imagen/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findOnePublicacionImagen", null);
__decorate([
    (0, common_1.Get)('categorias/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findOneCategorias", null);
__decorate([
    (0, common_1.Get)('publicacion-categoria/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "findOnePublicacionCategoria", null);
__decorate([
    (0, common_1.Delete)(':id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "remove", null);
__decorate([
    (0, common_1.Delete)('imagen/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "removeImagen", null);
__decorate([
    (0, common_1.Delete)('publicacion-imagen/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "removePublicacionImagen", null);
__decorate([
    (0, common_1.Delete)('categorias/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "removeCategorias", null);
__decorate([
    (0, common_1.Delete)('publicacion-categoria/:id'),
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('basic')),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PublicacionesController.prototype, "removePublicacionCategoria", null);
exports.PublicacionesController = PublicacionesController = __decorate([
    (0, common_1.Controller)('publicaciones'),
    __metadata("design:paramtypes", [publicaciones_business_impl_1.PublicacionesBusiness])
], PublicacionesController);
//# sourceMappingURL=publicaciones.controller.js.map