import { CreaPublicacionesDto } from '../dto/crea-publicacion.dto';
import { PublicacionesBusiness } from '../business/impl/publicaciones.business.impl';
import { CreaImagenDto } from '../dto/crea-imagen.dto';
import { CreaPublicacionImagenDto } from '../dto/crea-publicacion-imagen.dto';
import { CreaCategoriaDto } from '../dto/crea-categoria.dto';
import { CreaPublicacionCategoriaDto } from '../dto/crea-publicacion-categoria.dto';
export declare class PublicacionesController {
    private readonly publicacionesBusiness;
    constructor(publicacionesBusiness: PublicacionesBusiness);
    create(creaPublicacionesDto: CreaPublicacionesDto): Promise<import("../entities/publicacion.entity").publicaciones>;
    createImagen(creaImagenDto: CreaImagenDto): Promise<import("../entities/imagen.entity").imagenes>;
    createPublicacionImagen(creaPublicacionImagenDto: CreaPublicacionImagenDto): Promise<import("../entities/publicacion-imagen.entity").publicaciones_imagenes>;
    createCategorias(creaCategoriasDto: CreaCategoriaDto): Promise<import("../entities/categorias.entity").categorias>;
    createPublicacionCategoria(creaPublicacionCategoriasDto: CreaPublicacionCategoriaDto): Promise<import("../entities/publicaciones-categorias.entity").publicaciones_categorias>;
    findAll(): Promise<import("../entities/publicacion.entity").publicaciones[]>;
    findAllImagenes(): Promise<import("../entities/imagen.entity").imagenes[]>;
    findAllPublicacionesImagenes(): Promise<import("../entities/publicacion-imagen.entity").publicaciones_imagenes[]>;
    findAllCategorias(): Promise<import("../entities/categorias.entity").categorias[]>;
    findAllPublicacionCategoria(): Promise<import("../entities/publicaciones-categorias.entity").publicaciones_categorias[]>;
    findOne(id: string): Promise<import("../entities/publicacion.entity").publicaciones>;
    findOneImagen(id: string): Promise<import("../entities/imagen.entity").imagenes>;
    findOnePublicacionImagen(id: string): Promise<import("../entities/publicacion-imagen.entity").publicaciones_imagenes>;
    findOneCategorias(id: string): Promise<import("../entities/categorias.entity").categorias>;
    findOnePublicacionCategoria(id: string): Promise<import("../entities/publicaciones-categorias.entity").publicaciones_categorias>;
    remove(id: string): Promise<{
        affected?: number;
    }>;
    removeImagen(id: string): Promise<{
        affected?: number;
    }>;
    removePublicacionImagen(id: string): Promise<{
        affected?: number;
    }>;
    removeCategorias(id: string): Promise<{
        affected?: number;
    }>;
    removePublicacionCategoria(id: string): Promise<{
        affected?: number;
    }>;
}
