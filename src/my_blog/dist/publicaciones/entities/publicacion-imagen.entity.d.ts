import { publicaciones } from './publicacion.entity';
import { imagenes } from 'src/publicaciones/entities/imagen.entity';
export declare class publicaciones_imagenes {
    id: number;
    imagen: imagenes;
    publicacion: publicaciones;
}
