import { publicaciones_imagenes } from './publicacion-imagen.entity';
export declare class imagenes {
    id: number;
    url: string;
    descripcion: string;
    fecha_subida: Date;
    publicaciones_imagenes: publicaciones_imagenes[];
}
