"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicaciones = void 0;
const typeorm_1 = require("typeorm");
const publicacion_imagen_entity_1 = require("./publicacion-imagen.entity");
const publicaciones_categorias_entity_1 = require("./publicaciones-categorias.entity");
let publicaciones = class publicaciones {
};
exports.publicaciones = publicaciones;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], publicaciones.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    __metadata("design:type", String)
], publicaciones.prototype, "titulo", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'varchar', length: 255 }),
    __metadata("design:type", String)
], publicaciones.prototype, "subtitulo", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'text', nullable: true }),
    __metadata("design:type", String)
], publicaciones.prototype, "contenido", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' }),
    __metadata("design:type", Date)
], publicaciones.prototype, "fecha_creacion", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => publicacion_imagen_entity_1.publicaciones_imagenes, (publicaciones_imagenes) => publicaciones_imagenes.publicacion),
    __metadata("design:type", Array)
], publicaciones.prototype, "publicaciones_imagenes", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(() => publicaciones_categorias_entity_1.publicaciones_categorias, (publicaciones_categorias) => publicaciones_categorias.publicacion),
    __metadata("design:type", Array)
], publicaciones.prototype, "publicaciones_categorias", void 0);
exports.publicaciones = publicaciones = __decorate([
    (0, typeorm_1.Entity)()
], publicaciones);
//# sourceMappingURL=publicacion.entity.js.map