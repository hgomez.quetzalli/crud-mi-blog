import { publicaciones_imagenes } from './publicacion-imagen.entity';
import { publicaciones_categorias } from './publicaciones-categorias.entity';
export declare class publicaciones {
    id: number;
    titulo: string;
    subtitulo: string;
    contenido: string;
    fecha_creacion: Date;
    publicaciones_imagenes: publicaciones_imagenes[];
    publicaciones_categorias: publicaciones_categorias[];
}
