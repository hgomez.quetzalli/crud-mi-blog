import { publicaciones } from './publicacion.entity';
import { categorias } from './categorias.entity';
export declare class publicaciones_categorias {
    id: number;
    categoria: categorias;
    publicacion: publicaciones;
}
