"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicaciones_categorias = void 0;
const typeorm_1 = require("typeorm");
const publicacion_entity_1 = require("./publicacion.entity");
const categorias_entity_1 = require("./categorias.entity");
let publicaciones_categorias = class publicaciones_categorias {
};
exports.publicaciones_categorias = publicaciones_categorias;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], publicaciones_categorias.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => categorias_entity_1.categorias, (categoria) => categoria.publicaciones_categorias),
    (0, typeorm_1.JoinColumn)({ name: 'categoria_id' }),
    __metadata("design:type", categorias_entity_1.categorias)
], publicaciones_categorias.prototype, "categoria", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => publicacion_entity_1.publicaciones, (publicaciones) => publicaciones.publicaciones_imagenes),
    (0, typeorm_1.JoinColumn)({ name: 'publicacion_id' }),
    __metadata("design:type", publicacion_entity_1.publicaciones)
], publicaciones_categorias.prototype, "publicacion", void 0);
exports.publicaciones_categorias = publicaciones_categorias = __decorate([
    (0, typeorm_1.Entity)()
], publicaciones_categorias);
//# sourceMappingURL=publicaciones-categorias.entity.js.map