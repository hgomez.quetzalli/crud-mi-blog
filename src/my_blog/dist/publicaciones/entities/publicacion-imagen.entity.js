"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.publicaciones_imagenes = void 0;
const typeorm_1 = require("typeorm");
const publicacion_entity_1 = require("./publicacion.entity");
const imagen_entity_1 = require("./imagen.entity");
let publicaciones_imagenes = class publicaciones_imagenes {
};
exports.publicaciones_imagenes = publicaciones_imagenes;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], publicaciones_imagenes.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => imagen_entity_1.imagenes, (imagenes) => imagenes.publicaciones_imagenes),
    (0, typeorm_1.JoinColumn)({ name: 'imagen_id' }),
    __metadata("design:type", imagen_entity_1.imagenes)
], publicaciones_imagenes.prototype, "imagen", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => publicacion_entity_1.publicaciones, (publicaciones) => publicaciones.publicaciones_imagenes),
    (0, typeorm_1.JoinColumn)({ name: 'publicacion_id' }),
    __metadata("design:type", publicacion_entity_1.publicaciones)
], publicaciones_imagenes.prototype, "publicacion", void 0);
exports.publicaciones_imagenes = publicaciones_imagenes = __decorate([
    (0, typeorm_1.Entity)()
], publicaciones_imagenes);
//# sourceMappingURL=publicacion-imagen.entity.js.map