import { publicaciones_categorias } from './publicaciones-categorias.entity';
export declare class categorias {
    id: number;
    nombre: string;
    publicaciones_categorias: publicaciones_categorias[];
}
