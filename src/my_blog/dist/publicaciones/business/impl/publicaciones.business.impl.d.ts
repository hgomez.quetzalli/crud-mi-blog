import { IPublicacionesBusiness } from "../publicaciones.business";
import { IPublicacionesService } from "src/publicaciones/services/publicaciones.service";
import { CreaPublicacionesDto } from "src/publicaciones/dto/crea-publicacion.dto";
import { CreaImagenDto } from "src/publicaciones/dto/crea-imagen.dto";
import { publicaciones } from "src/publicaciones/entities/publicacion.entity";
import { imagenes } from "src/publicaciones/entities/imagen.entity";
import { CreaPublicacionImagenDto } from "src/publicaciones/dto/crea-publicacion-imagen.dto";
import { publicaciones_imagenes } from "src/publicaciones/entities/publicacion-imagen.entity";
import { CreaCategoriaDto } from "src/publicaciones/dto/crea-categoria.dto";
import { categorias } from "src/publicaciones/entities/categorias.entity";
import { CreaPublicacionCategoriaDto } from "src/publicaciones/dto/crea-publicacion-categoria.dto";
import { publicaciones_categorias } from "src/publicaciones/entities/publicaciones-categorias.entity";
export declare class PublicacionesBusiness implements IPublicacionesBusiness {
    private readonly publicacionesService;
    constructor(publicacionesService: IPublicacionesService);
    create(creaPublicacionesDto: CreaPublicacionesDto): Promise<publicaciones>;
    createImagenes(creaImagenDto: CreaImagenDto): Promise<imagenes>;
    createPublicacionImagen(creaPublicaionImagenDto: CreaPublicacionImagenDto): Promise<publicaciones_imagenes>;
    createCategoria(creaCategoriaDto: CreaCategoriaDto): Promise<categorias>;
    createPublicacionCategoria(creaPublicacionCategoriaDto: CreaPublicacionCategoriaDto): Promise<publicaciones_categorias>;
    findAll(): Promise<publicaciones[]>;
    findAllImagenes(): Promise<imagenes[]>;
    findAllPublicacionesImagenes(): Promise<publicaciones_imagenes[]>;
    findAllCategorias(): Promise<categorias[]>;
    findAllPublicacionCategorias(): Promise<publicaciones_categorias[]>;
    findOne(id: number): Promise<publicaciones>;
    findOneImagen(id: number): Promise<imagenes>;
    findOnePublicacionImagen(id: number): Promise<publicaciones_imagenes>;
    findOneCategoria(id: number): Promise<categorias>;
    findOnePublicacionCategoria(id: number): Promise<publicaciones_categorias>;
    remove(id: number): Promise<{
        affected?: number;
    }>;
    removeImagen(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionImagen(id: number): Promise<{
        affected?: number;
    }>;
    removeCategorias(id: number): Promise<{
        affected?: number;
    }>;
    removePublicacionCategorias(id: number): Promise<{
        affected?: number;
    }>;
}
