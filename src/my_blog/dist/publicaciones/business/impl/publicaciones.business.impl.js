"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicacionesBusiness = void 0;
const common_1 = require("@nestjs/common");
let PublicacionesBusiness = class PublicacionesBusiness {
    constructor(publicacionesService) {
        this.publicacionesService = publicacionesService;
    }
    create(creaPublicacionesDto) {
        return this.publicacionesService.create(creaPublicacionesDto);
    }
    createImagenes(creaImagenDto) {
        return this.publicacionesService.createImagen(creaImagenDto);
    }
    createPublicacionImagen(creaPublicaionImagenDto) {
        return this.publicacionesService.createPublicacionImagen(creaPublicaionImagenDto);
    }
    createCategoria(creaCategoriaDto) {
        return this.publicacionesService.createCategoria(creaCategoriaDto);
    }
    createPublicacionCategoria(creaPublicacionCategoriaDto) {
        return this.publicacionesService.createPublicacionCategoria(creaPublicacionCategoriaDto);
    }
    findAll() {
        return this.publicacionesService.findAll();
    }
    findAllImagenes() {
        return this.publicacionesService.findAllImagenes();
    }
    findAllPublicacionesImagenes() {
        return this.publicacionesService.findAllPublicacionImagenes();
    }
    findAllCategorias() {
        return this.publicacionesService.findAllCategorias();
    }
    findAllPublicacionCategorias() {
        return this.publicacionesService.findAllPublicacionCategorias();
    }
    findOne(id) {
        return this.publicacionesService.findOne(id);
    }
    findOneImagen(id) {
        return this.publicacionesService.findOneImagen(id);
    }
    findOnePublicacionImagen(id) {
        return this.publicacionesService.findOnePublicacionImagen(id);
    }
    findOneCategoria(id) {
        return this.publicacionesService.findOneCategoria(id);
    }
    findOnePublicacionCategoria(id) {
        return this.publicacionesService.findOnePublicacionCategoria(id);
    }
    remove(id) {
        return this.publicacionesService.remove(id);
    }
    removeImagen(id) {
        return this.publicacionesService.removeImagen(id);
    }
    removePublicacionImagen(id) {
        return this.publicacionesService.removePublicacionImagen(id);
    }
    removeCategorias(id) {
        return this.publicacionesService.removeCategorias(id);
    }
    removePublicacionCategorias(id) {
        return this.publicacionesService.removePublicacionCategorias(id);
    }
};
exports.PublicacionesBusiness = PublicacionesBusiness;
exports.PublicacionesBusiness = PublicacionesBusiness = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)('IPublicacionesService')),
    __metadata("design:paramtypes", [Object])
], PublicacionesBusiness);
//# sourceMappingURL=publicaciones.business.impl.js.map