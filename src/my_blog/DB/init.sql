CREATE SCHEMA mi_blog_schema;

SET search_path TO mi_blog_schema;

-- Publicaciones
CREATE TABLE publicaciones (
    id SERIAL PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    subtitulo VARCHAR(255),
    contenido TEXT NOT NULL,
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Imágenes
CREATE TABLE imagenes (
    id SERIAL PRIMARY KEY,
    url VARCHAR(255) NOT NULL,
    descripcion TEXT,
    fecha_subida TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Publicaciones_Imagenes
CREATE TABLE publicaciones_imagenes (
    id SERIAL PRIMARY KEY,
    publicacion_id INT REFERENCES publicaciones(id),
    imagen_id INT REFERENCES imagenes(id)
);

-- Categorías
CREATE TABLE categorias (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(100) NOT NULL
);

-- Publicaciones_Categorias
CREATE TABLE publicaciones_categorias (
    id SERIAL PRIMARY KEY,
    publicacion_id INT REFERENCES publicaciones(id),
    categoria_id INT REFERENCES categorias(id)
);

-- Enlaces
CREATE TABLE enlaces (
    id SERIAL PRIMARY KEY,
    titulo VARCHAR(255) NOT NULL,
    url VARCHAR(255) NOT NULL
);

