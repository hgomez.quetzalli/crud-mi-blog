import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConfigService } from '@nestjs/config';
import { config } from 'dotenv'; 
import { PublicacionesModule } from './publicaciones/publicaciones.module';
import { publicaciones } from './publicaciones/entities/publicacion.entity';
import { imagenes } from './publicaciones/entities/imagen.entity';
import { publicaciones_imagenes } from './publicaciones/entities/publicacion-imagen.entity';
import { categorias } from './publicaciones/entities/categorias.entity';
import { publicaciones_categorias } from './publicaciones/entities/publicaciones-categorias.entity';
import { EnlacesModule } from './enlaces/enlaces.module';
import { Enlace } from './enlaces/entities/enlace.entity';
import { AuthModule } from './auth/auth.module';

//config() carga las variables de entorno desde un archivo .env si existe.
config();

//Se crea una instancia de ConfigService para facilitar el acceso a las variables de entorno.
const configService = new ConfigService();

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: configService.get('POSTGRES_HOST'),
      port: configService.get('POSTGRES_PORT'),
      username: configService.get('POSTGRES_USER'),
      password: configService.get('POSTGRES_PASSWORD'),
      database: configService.get('POSTGRES_DATABASE'),
      entities: [ publicaciones, imagenes, publicaciones_imagenes, categorias, publicaciones_categorias, Enlace],
      synchronize: true,
    }),
    PublicacionesModule,
    EnlacesModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
