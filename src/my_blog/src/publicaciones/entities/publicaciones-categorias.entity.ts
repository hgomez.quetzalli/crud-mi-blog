import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm'
import { publicaciones } from './publicacion.entity';
import { categorias } from './categorias.entity';

@Entity()
export class publicaciones_categorias {

    /**
     * este decorador ayudara a autogenerar el id para la tabla
     */
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => categorias, (categoria) => categoria.publicaciones_categorias)
    @JoinColumn({ name: 'categoria_id' })
    categoria: categorias;

    @ManyToOne(() => publicaciones, (publicaciones) => publicaciones.publicaciones_imagenes)
    @JoinColumn({ name: 'publicacion_id' })
    publicacion: publicaciones;
    
}