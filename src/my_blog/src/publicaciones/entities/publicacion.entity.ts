import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { publicaciones_imagenes } from './publicacion-imagen.entity';
import { publicaciones_categorias } from './publicaciones-categorias.entity';

@Entity()
export class publicaciones {

    /**
     * este decorador ayudara a autogenerar el id para la tabla
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length:255 })
    titulo: string;

    @Column({ type: 'varchar', length:255 })
    subtitulo: string;

    @Column({ type: 'text', nullable: true })
    contenido: string;

    @Column({ type: 'timestamp', default:() => 'CURRENT_TIMESTAMP' })
    fecha_creacion: Date;

    @OneToMany(() => publicaciones_imagenes, (publicaciones_imagenes) => publicaciones_imagenes.publicacion)
    publicaciones_imagenes: publicaciones_imagenes[];

    @OneToMany(() => publicaciones_categorias, (publicaciones_categorias) => publicaciones_categorias.publicacion)
    publicaciones_categorias: publicaciones_categorias[];

}