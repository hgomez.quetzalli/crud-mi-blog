import { Column, Entity, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm'
import { publicaciones } from './publicacion.entity';
import { imagenes } from 'src/publicaciones/entities/imagen.entity';

@Entity()
export class publicaciones_imagenes {

    /**
     * este decorador ayudara a autogenerar el id para la tabla
     */
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => imagenes, (imagenes) => imagenes.publicaciones_imagenes)
    @JoinColumn({ name: 'imagen_id' })
    imagen: imagenes;

    @ManyToOne(() => publicaciones, (publicaciones) => publicaciones.publicaciones_imagenes)
    @JoinColumn({ name: 'publicacion_id' })
    publicacion: publicaciones;
    
}