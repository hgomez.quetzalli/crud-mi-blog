import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { publicaciones_categorias } from './publicaciones-categorias.entity';

@Entity()
export class categorias {

    /**
     * este decorador ayudara a autogenerar el id para la tabla
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length:100 })
    nombre: string;

    @OneToMany(() => publicaciones_categorias, (publicaciones_categorias) => publicaciones_categorias.categoria)
    publicaciones_categorias: publicaciones_categorias[];

}