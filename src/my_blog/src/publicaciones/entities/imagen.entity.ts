import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import { publicaciones_imagenes } from './publicacion-imagen.entity';

@Entity()
export class imagenes {

    /**
     * este decorador ayudara a autogenerar el id para la tabla
     */
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length:255 })
    url: string;

    @Column({ type: 'text', nullable: true })
    descripcion: string;

    @Column({ type: 'timestamp', default:() => 'CURRENT_TIMESTAMP' })
    fecha_subida: Date;

    @OneToMany(() => publicaciones_imagenes, (publicaciones_imagenes) => publicaciones_imagenes.imagen)
    publicaciones_imagenes: publicaciones_imagenes[];
}