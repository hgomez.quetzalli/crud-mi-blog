import { Injectable } from '@nestjs/common';
import { InjectRepository  } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IPublicacionesService } from '../publicaciones.service';


import { publicaciones } from '../../entities/publicacion.entity';
import { imagenes } from 'src/publicaciones/entities/imagen.entity';
import { publicaciones_imagenes } from 'src/publicaciones/entities/publicacion-imagen.entity';

import { CreaImagenDto } from 'src/publicaciones/dto/crea-imagen.dto';
import { CreaPublicacionImagenDto } from 'src/publicaciones/dto/crea-publicacion-imagen.dto';
import { CreaPublicacionesDto } from '../../dto/crea-publicacion.dto';
import { CreaCategoriaDto } from 'src/publicaciones/dto/crea-categoria.dto';
import { categorias } from 'src/publicaciones/entities/categorias.entity';
import { CreaPublicacionCategoriaDto } from 'src/publicaciones/dto/crea-publicacion-categoria.dto';
import { publicaciones_categorias } from 'src/publicaciones/entities/publicaciones-categorias.entity';
//import { CreatePublicacioneDto } from './dto/create-publicacione.dto';
//import { UpdatePublicacioneDto } from './dto/update-publicacione.dto';

@Injectable()
export class PublicacionesService implements IPublicacionesService{

  
  constructor(
    @InjectRepository( publicaciones ) private readonly publicacionesRepository:Repository<publicaciones>,
    @InjectRepository( imagenes ) private readonly imagenesRepository:Repository<imagenes>,
    @InjectRepository( publicaciones_imagenes ) private readonly publicacionesImagenesRepository:Repository<publicaciones_imagenes>,
    @InjectRepository( categorias ) private readonly categoriasRepository:Repository<categorias>,
    @InjectRepository( publicaciones_categorias ) private readonly publicacionesCategoriasRepository:Repository<publicaciones_categorias>,
  ){}

  create(creaPublicacionesDto: CreaPublicacionesDto): Promise<publicaciones> {
    const publica: publicaciones = new publicaciones();
    publica.titulo = creaPublicacionesDto.titulo;
    publica.subtitulo = creaPublicacionesDto.subtitulo;
    publica.contenido = creaPublicacionesDto.contenido;
    publica.fecha_creacion = creaPublicacionesDto.fecha_creacion;
    return this.publicacionesRepository.save(publica);
  }

  createImagen(creaImagenDto: CreaImagenDto): Promise<imagenes>{
    const ima: imagenes = new imagenes();
    ima.url = creaImagenDto.url;
    ima.descripcion = creaImagenDto.descripcion;
    ima.fecha_subida = creaImagenDto.fecha_subida;
    return this.imagenesRepository.save(ima);
  }

  async createPublicacionImagen(creaPublicacionImagenDto: CreaPublicacionImagenDto): Promise<publicaciones_imagenes>{
    const nuevaPublicacionImagen: publicaciones_imagenes = new publicaciones_imagenes();
    const idImagen = creaPublicacionImagenDto.imagen_id;
    nuevaPublicacionImagen.imagen = await this.imagenesRepository.findOneBy({id: idImagen});
    const idPub = creaPublicacionImagenDto.publicacion_id;
    nuevaPublicacionImagen.publicacion = await this.publicacionesRepository.findOneBy({id: idPub});
    return this.publicacionesImagenesRepository.save(nuevaPublicacionImagen);
  }

  createCategoria(creaCategoriaDto: CreaCategoriaDto): Promise<categorias>{
    const cat : categorias = new categorias();
    cat.nombre = creaCategoriaDto.nombre;
    return this.categoriasRepository.save(cat);
  }

  async createPublicacionCategoria(creaPublicacionCategoriaDto: CreaPublicacionCategoriaDto): Promise<publicaciones_categorias>{
    const nuevaPublicacionCategoria: publicaciones_categorias = new publicaciones_categorias();
    const idImagen = creaPublicacionCategoriaDto.categoria_id;
    nuevaPublicacionCategoria.categoria = await this.categoriasRepository.findOneBy({id: idImagen});
    const idPub = creaPublicacionCategoriaDto.publicacion_id;
    nuevaPublicacionCategoria.publicacion = await this.publicacionesRepository.findOneBy({id: idPub});
    return this.publicacionesCategoriasRepository.save(nuevaPublicacionCategoria);
  }

  findAll(): Promise<publicaciones[]> {
    return this.publicacionesRepository.find();
  }
  
  findAllImagenes(): Promise<imagenes[]> {
    return this.imagenesRepository.find();
  }

  findAllPublicacionImagenes(): Promise<publicaciones_imagenes[]>{
    return this.publicacionesImagenesRepository.find({ relations: ['imagen', 'publicacion'] });
  }

  findAllCategorias(): Promise<categorias[]>{
    return this.categoriasRepository.find();
  }
  findAllPublicacionCategorias(): Promise<publicaciones_categorias[]>{
    return this.publicacionesCategoriasRepository.find({ relations: ['categoria', 'publicacion'] });
  }

  findOne(id: number): Promise<publicaciones> {
    return this.publicacionesRepository.findOneBy({id});
  }

  findOneImagen(id: number): Promise<imagenes> {
    return this.imagenesRepository.findOneBy({id});
  }

  async findOnePublicacionImagen(id: number): Promise<publicaciones_imagenes>{
    const publicacionImagen = await this.publicacionesImagenesRepository.findOne({
      where: { id },
      relations: ['imagen', 'publicacion'],
    });
  return publicacionImagen; 
  }

  findOneCategoria(id: number): Promise<categorias>{
    return this.categoriasRepository.findOneBy({id});
  }

  async findOnePublicacionCategoria(id: number): Promise<publicaciones_categorias>{
    const publicacionCategoria = await this.publicacionesCategoriasRepository.findOne({
      where: { id },
      relations: ['categoria', 'publicacion'],
    });
  return publicacionCategoria; 
  }

  /*
  update(id: number, updatePublicacioneDto: UpdatePublicacioneDto) {
    return `This action updates a #${id} publicacione`;
  }
  */

  remove(id: number): Promise<{affected?: number}> {
    return this.publicacionesRepository.delete(id);
  }

  removeImagen(id: number): Promise<{affected?: number}> {
    return this.imagenesRepository.delete(id);
  }

  removePublicacionImagen(id: number): Promise<{affected?: number}>{
    return this.publicacionesImagenesRepository.delete(id);
  }

  removeCategorias(id: number): Promise<{affected?: number}>{
    return this.categoriasRepository.delete(id);
  }
  
  removePublicacionCategorias(id: number): Promise<{affected?: number}>{
    return this.publicacionesCategoriasRepository.delete(id);
  }

}
