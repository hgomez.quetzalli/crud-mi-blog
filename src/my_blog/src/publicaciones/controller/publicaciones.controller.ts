import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreaPublicacionesDto } from '../dto/crea-publicacion.dto';
import { PublicacionesBusiness } from '../business/impl/publicaciones.business.impl';
import { CreaImagenDto } from '../dto/crea-imagen.dto';
import { CreaPublicacionImagenDto } from '../dto/crea-publicacion-imagen.dto';
import { CreaCategoriaDto } from '../dto/crea-categoria.dto';
import { CreaPublicacionCategoriaDto } from '../dto/crea-publicacion-categoria.dto';

@Controller('publicaciones')
export class PublicacionesController {
  constructor(private readonly publicacionesBusiness: PublicacionesBusiness) {}

  @Post()
  @UseGuards(AuthGuard('basic'))
  create(@Body() creaPublicacionesDto:CreaPublicacionesDto) {
    return this.publicacionesBusiness.create(creaPublicacionesDto);
  }

  @Post('imagen')
  @UseGuards(AuthGuard('basic'))
  createImagen(@Body() creaImagenDto:CreaImagenDto) {
    return this.publicacionesBusiness.createImagenes(creaImagenDto);
  }

  @Post('publicacion-imagen')
  @UseGuards(AuthGuard('basic'))
  createPublicacionImagen(@Body() creaPublicacionImagenDto:CreaPublicacionImagenDto) {
    return this.publicacionesBusiness.createPublicacionImagen(creaPublicacionImagenDto);
  }

  @Post('categorias')
  @UseGuards(AuthGuard('basic'))
  createCategorias(@Body() creaCategoriasDto:CreaCategoriaDto) {
    return this.publicacionesBusiness.createCategoria(creaCategoriasDto);
  }

  @Post('publicacion-categoria')
  @UseGuards(AuthGuard('basic'))
  createPublicacionCategoria(@Body() creaPublicacionCategoriasDto:CreaPublicacionCategoriaDto) {
    return this.publicacionesBusiness.createPublicacionCategoria(creaPublicacionCategoriasDto);
  }
  
  @Get()
  @UseGuards(AuthGuard('basic'))
  findAll() {
    return this.publicacionesBusiness.findAll();
  }

  @Get('imagen')
  @UseGuards(AuthGuard('basic'))
  findAllImagenes() {
    return this.publicacionesBusiness.findAllImagenes();
  }

  @Get('publicacion-imagen')
  @UseGuards(AuthGuard('basic'))
  findAllPublicacionesImagenes() {
    return this.publicacionesBusiness.findAllPublicacionesImagenes();
  }

  @Get('categorias')
  @UseGuards(AuthGuard('basic'))
  findAllCategorias() {
    return this.publicacionesBusiness.findAllCategorias();
  }

  @Get('publicacion-categoria')
  @UseGuards(AuthGuard('basic'))
  findAllPublicacionCategoria() {
    return this.publicacionesBusiness.findAllPublicacionCategorias();
  }

  @Get(':id')
  @UseGuards(AuthGuard('basic'))
  findOne(@Param('id') id: string) {
    return this.publicacionesBusiness.findOne(+id);
  }

  @Get('imagen/:id')
  @UseGuards(AuthGuard('basic'))
  findOneImagen(@Param('id') id: string) {
    return this.publicacionesBusiness.findOneImagen(+id);
  }

  @Get('publicacion-imagen/:id')
  @UseGuards(AuthGuard('basic'))
  findOnePublicacionImagen(@Param('id') id: string) {
    return this.publicacionesBusiness.findOnePublicacionImagen(+id);
  }

  @Get('categorias/:id')
  @UseGuards(AuthGuard('basic'))
  findOneCategorias(@Param('id') id: string) {
    return this.publicacionesBusiness.findOneCategoria(+id);
  }

  @Get('publicacion-categoria/:id')
  @UseGuards(AuthGuard('basic'))
  findOnePublicacionCategoria(@Param('id') id: string) {
    return this.publicacionesBusiness.findOnePublicacionCategoria(+id);
  }

  /*@Patch(':id')
  update(@Param('id') id: string, @Body() updatePublicacioneDto: UpdatePublicacioneDto) {
    return this.publicacionesService.update(+id, updatePublicacioneDto);
  }*/

  @Delete(':id')
  @UseGuards(AuthGuard('basic'))
  remove(@Param('id') id: string) {
    return this.publicacionesBusiness.remove(+id);
  }

  @Delete('imagen/:id')
  @UseGuards(AuthGuard('basic'))
  removeImagen(@Param('id') id: string) {
    return this.publicacionesBusiness.removeImagen(+id);
  }

  @Delete('publicacion-imagen/:id')
  @UseGuards(AuthGuard('basic'))
  removePublicacionImagen(@Param('id') id: string) {
    return this.publicacionesBusiness.removePublicacionImagen(+id);
  }

  @Delete('categorias/:id')
  @UseGuards(AuthGuard('basic'))
  removeCategorias(@Param('id') id: string) {
    return this.publicacionesBusiness.removeCategorias(+id);
  }

  @Delete('publicacion-categoria/:id')
  @UseGuards(AuthGuard('basic'))
  removePublicacionCategoria(@Param('id') id: string) {
    return this.publicacionesBusiness.removePublicacionCategorias(+id);
  }
}
