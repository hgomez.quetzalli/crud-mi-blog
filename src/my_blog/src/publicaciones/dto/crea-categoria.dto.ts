import {
    IsAlphanumeric,
    IsEmail,
    IsEnum,
    IsInt,
    IsNotEmpty,
    IsString,
    Matches,
    MinLength,
} from 'class-validator';

export class CreaCategoriaDto {

    @IsString()
    @IsNotEmpty()
    nombre: string;
}