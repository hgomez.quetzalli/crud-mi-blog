import {
    IsAlphanumeric,
    IsEmail,
    IsEnum,
    IsInt,
    IsNotEmpty,
    IsString,
    Matches,
    MinLength,
} from 'class-validator';

export class CreaImagenDto {

    @IsString()
    @IsNotEmpty()
    url: string;

    @IsString()
    descripcion: string;

    @IsNotEmpty()
    fecha_subida: Date;
}