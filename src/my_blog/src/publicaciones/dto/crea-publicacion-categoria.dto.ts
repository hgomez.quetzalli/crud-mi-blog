import {
    IsAlphanumeric,
    IsEmail,
    IsEnum,
    IsInt,
    IsNotEmpty,
    IsString,
    Matches,
    MinLength,
} from 'class-validator';

export class CreaPublicacionCategoriaDto {
    @IsInt()
    @IsNotEmpty()
    publicacion_id: number;
    
    @IsInt()
    @IsNotEmpty()
    categoria_id: number;
}