import {
    IsAlphanumeric,
    IsEmail,
    IsEnum,
    IsInt,
    IsNotEmpty,
    IsString,
    Matches,
    MinLength,
} from 'class-validator';

export class CreaPublicacionesDto {

    @IsString()
    @IsNotEmpty()
    titulo: string;

    @IsString()
    subtitulo: string;

    @IsString()
    @IsNotEmpty()
    contenido: string;

    @IsNotEmpty()
    fecha_creacion: Date;
}