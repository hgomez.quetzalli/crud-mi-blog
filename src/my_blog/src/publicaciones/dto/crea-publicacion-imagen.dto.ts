import {
    IsAlphanumeric,
    IsEmail,
    IsEnum,
    IsInt,
    IsNotEmpty,
    IsString,
    Matches,
    MinLength,
} from 'class-validator';

export class CreaPublicacionImagenDto {
    @IsInt()
    @IsNotEmpty()
    publicacion_id: number;
    
    @IsInt()
    @IsNotEmpty()
    imagen_id: number;
}