import { Module } from '@nestjs/common';
import { PublicacionesService } from './services/impl/publicaciones.service.impl';
import { PublicacionesController } from './controller/publicaciones.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { publicaciones } from './entities/publicacion.entity';
import { imagenes } from './entities/imagen.entity'; 
import { PublicacionesBusiness } from './business/impl/publicaciones.business.impl';
import { publicaciones_imagenes } from './entities/publicacion-imagen.entity';
import { categorias } from './entities/categorias.entity';
import { publicaciones_categorias } from './entities/publicaciones-categorias.entity';

@Module({
  imports: [TypeOrmModule.forFeature([publicaciones, imagenes, publicaciones_imagenes, categorias, publicaciones_categorias])],
  controllers: [PublicacionesController],
  providers: [PublicacionesBusiness, {provide: 'IPublicacionesService', useClass: PublicacionesService}],
})
export class PublicacionesModule {}
