import { Inject, Injectable } from "@nestjs/common";
import { IPublicacionesBusiness } from "../publicaciones.business";
import { IPublicacionesService } from "src/publicaciones/services/publicaciones.service";
import { CreaPublicacionesDto } from "src/publicaciones/dto/crea-publicacion.dto";
import { CreaImagenDto } from "src/publicaciones/dto/crea-imagen.dto";
import { publicaciones } from "src/publicaciones/entities/publicacion.entity";
import { imagenes } from "src/publicaciones/entities/imagen.entity";
import { CreaPublicacionImagenDto } from "src/publicaciones/dto/crea-publicacion-imagen.dto";
import { publicaciones_imagenes } from "src/publicaciones/entities/publicacion-imagen.entity";
import { CreaCategoriaDto } from "src/publicaciones/dto/crea-categoria.dto";
import { categorias } from "src/publicaciones/entities/categorias.entity";
import { CreaPublicacionCategoriaDto } from "src/publicaciones/dto/crea-publicacion-categoria.dto";
import { publicaciones_categorias } from "src/publicaciones/entities/publicaciones-categorias.entity";

@Injectable()
export class PublicacionesBusiness implements IPublicacionesBusiness {

    constructor(
        @Inject('IPublicacionesService')private readonly publicacionesService: IPublicacionesService
    ){}

    create(creaPublicacionesDto: CreaPublicacionesDto): Promise<publicaciones>{
        return this.publicacionesService.create(creaPublicacionesDto);
    }
    
    createImagenes(creaImagenDto: CreaImagenDto): Promise<imagenes>{
        return this.publicacionesService.createImagen(creaImagenDto);
    }

    createPublicacionImagen(creaPublicaionImagenDto: CreaPublicacionImagenDto): Promise<publicaciones_imagenes>{
        return this.publicacionesService.createPublicacionImagen(creaPublicaionImagenDto);
    }

    createCategoria(creaCategoriaDto: CreaCategoriaDto): Promise<categorias>{
        return this.publicacionesService.createCategoria(creaCategoriaDto);
    }

    createPublicacionCategoria(creaPublicacionCategoriaDto: CreaPublicacionCategoriaDto): Promise<publicaciones_categorias>{
        return this.publicacionesService.createPublicacionCategoria(creaPublicacionCategoriaDto);
    }

    findAll(): Promise<publicaciones[]>{
        return this.publicacionesService.findAll();
    }

    findAllImagenes(): Promise<imagenes[]>{
        return this.publicacionesService.findAllImagenes();
    }

    findAllPublicacionesImagenes(): Promise<publicaciones_imagenes[]>{
        return this.publicacionesService.findAllPublicacionImagenes();
    }

    findAllCategorias(): Promise<categorias[]>{
        return this.publicacionesService.findAllCategorias();
    }

    findAllPublicacionCategorias(): Promise<publicaciones_categorias[]>{
        return this.publicacionesService.findAllPublicacionCategorias();
    }

    findOne(id: number): Promise<publicaciones>{
        return this.publicacionesService.findOne(id);
    }

    findOneImagen(id: number): Promise<imagenes>{
        return this.publicacionesService.findOneImagen(id);
    }

    findOnePublicacionImagen(id: number): Promise<publicaciones_imagenes>{
        return this.publicacionesService.findOnePublicacionImagen(id);
    }

    findOneCategoria(id: number): Promise<categorias>{
        return this.publicacionesService.findOneCategoria(id);
    }

    findOnePublicacionCategoria(id: number): Promise<publicaciones_categorias>{
        return this.publicacionesService.findOnePublicacionCategoria(id);
    }
    //update(id: number, updatePublicacioneDto: UpdatePublicacioneDto);
    remove(id: number): Promise<{affected?: number}>{
        return this.publicacionesService.remove(id);
    }

    removeImagen(id: number): Promise<{affected?: number}>{
        return this.publicacionesService.removeImagen(id);
    }

    removePublicacionImagen(id: number): Promise<{affected?: number}>{
        return this.publicacionesService.removePublicacionImagen(id);
    }

    removeCategorias(id: number): Promise<{affected?: number}>{
        return this.publicacionesService.removeCategorias(id);
    }

    removePublicacionCategorias(id: number): Promise<{affected?: number}>{
        return this.publicacionesService.removePublicacionCategorias(id);
    }
}