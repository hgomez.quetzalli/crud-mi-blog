export enum LogServices {
    Postgres = "Postgres"
}

export enum LogType{
    Request = "REQUEST",
    Response = "RESPONSE",
    Payload = "PAYLOAD",
    Error = "ERROR"
}