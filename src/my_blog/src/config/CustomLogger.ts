import { ConsoleLogger, Injectable, Scope } from "@nestjs/common";
import { isEmpty } from "lodash";
import { LogServices, LogType } from "src/common/model/types/customLogger";
import { v4 as uuid4 } from 'uuid';

type LogOptions = {
    id?: string | number;
    type?: LogType;
    message?: string;
    payload?: any;
};

// scope es un parametro en este caso se establece el alcance (scope) del servicio en 'REQUEST' que establece se debe crear
//una nueva instancia sel servicio para cada solicitud HTTP entrante
@Injectable({ scope: Scope.REQUEST})
export class CustomLogger extends ConsoleLogger {

    readonly trackingId: string;

    constructor(){
        super();
        this.trackingId = uuid4();
    }

    private buildLog(serviceName: LogServices, options?: LogOptions) {
        /*
        options || {}: se conoce como el operador de fusion null (||). Si options es null o undefined, se usa el objeto vacio 
        {}
        {id : customId, type= LogType.Payload, message, payload} cada propiedad dentro de las llaves se extraera del objeto 
        options (o del vacio si options es null o undefined) y se le asignara a una variable con el mismo nombre o a una 
        variable con un nombre personalizado
        */
        const {id : customId, type= LogType.Payload, message, payload} = options || {};

        const formattedId = customId ? ` (${customId})` : '';
        const fornattedMessage = message ? ` ${message}` : '';
        const formattedPayload = isEmpty(payload) ? '' : `: ${JSON.stringify(payload)}`;

        return [`[${serviceName}] [${this.trackingId}] [${type}]`, formattedId, fornattedMessage, formattedPayload].join('');
    }

    serviceLog(serviceName: LogServices, options?: LogOptions) {
        super.log(this.buildLog(serviceName, options));
    }

    serviceError(serviceName: LogServices, options?: LogOptions) {
        super.error(this.buildLog(serviceName, options));
    }
}