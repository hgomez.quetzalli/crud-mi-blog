import { CreateEnlaceDto } from "../dto/create-enlace.dto";
import { UpdateEnlaceDto } from "../dto/update-enlace.dto";
import { Enlace } from "../entities/enlace.entity";

export interface IEnlaceBusiness {

    create(creaPublicacionesDto: CreateEnlaceDto): Promise<Enlace>;
    findAll(): Promise<Enlace[]>;
    findOne(id: number): Promise<Enlace>;
    update(id: number, updateEnlaceDto: UpdateEnlaceDto): Promise<Enlace>;
    remove(id: number): Promise<{affected?: number}>;

}