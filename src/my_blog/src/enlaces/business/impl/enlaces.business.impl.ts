import { Inject, Injectable } from "@nestjs/common";
import { IEnlaceBusiness } from "../enlaces.business";
import { IEnlaceService } from "src/enlaces/services/enlaces.service";
import { CreateEnlaceDto } from "src/enlaces/dto/create-enlace.dto";
import { Enlace } from "src/enlaces/entities/enlace.entity";
import { UpdateEnlaceDto } from "src/enlaces/dto/update-enlace.dto";

@Injectable()
export class EnlaceBusiness implements IEnlaceBusiness {

    constructor(
        @Inject('IEnlacesService')private readonly enlacesService: IEnlaceService
    ){}

    create(createEnlaceDto: CreateEnlaceDto): Promise<Enlace>{
        return this.enlacesService.create(createEnlaceDto);
    }

    findAll(): Promise<Enlace[]>{
        return this.enlacesService.findAll();
    }

    findOne(id: number): Promise<Enlace>{
        return this.enlacesService.findOne(id);
    }

    update(id: number, updatePublicacioneDto: UpdateEnlaceDto): Promise<Enlace>{
        return this.enlacesService.update(id, updatePublicacioneDto);
    }

    remove(id: number): Promise<{affected?: number}>{
        return this.enlacesService.remove(id);
    }

}