import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { CreateEnlaceDto } from '../dto/create-enlace.dto';
import { UpdateEnlaceDto } from '../dto/update-enlace.dto';
import { EnlaceBusiness } from '../business/impl/enlaces.business.impl';
import { AuthGuard } from '@nestjs/passport';

@Controller('enlaces')
export class EnlacesController {
  constructor(private readonly enlacesBusiness: EnlaceBusiness) {}

  @Post()
  @UseGuards(AuthGuard('basic'))
  create(@Body() createEnlaceDto: CreateEnlaceDto) {
    return this.enlacesBusiness.create(createEnlaceDto);
  }

  @Get()
  @UseGuards(AuthGuard('basic'))
  findAll() {
    return this.enlacesBusiness.findAll();
  }

  @Get(':id')
  @UseGuards(AuthGuard('basic'))
  findOne(@Param('id') id: string) {
    return this.enlacesBusiness.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard('basic'))
  update(@Param('id') id: string, @Body() updateEnlaceDto: UpdateEnlaceDto) {
    return this.enlacesBusiness.update(+id, updateEnlaceDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('basic'))
  remove(@Param('id') id: string) {
    return this.enlacesBusiness.remove(+id);
  }
}
