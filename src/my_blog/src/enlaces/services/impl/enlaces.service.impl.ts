import { Injectable } from '@nestjs/common';
import { InjectRepository  } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEnlaceDto } from '../../dto/create-enlace.dto';
import { UpdateEnlaceDto } from '../../dto/update-enlace.dto';
import { Enlace } from 'src/enlaces/entities/enlace.entity';
import { IEnlaceService } from '../enlaces.service';

@Injectable()
export class EnlacesService implements IEnlaceService{

  constructor(
    @InjectRepository( Enlace ) private readonly enlaceRepository:Repository<Enlace>,
  ){}

  create(createEnlaceDto: CreateEnlaceDto): Promise<Enlace> {
    const enlaceEntity: Enlace = new Enlace();
    enlaceEntity.titulo = createEnlaceDto.titulo;
    enlaceEntity.url = createEnlaceDto.url;
    return this.enlaceRepository.save(enlaceEntity);
  }

  findAll(): Promise<Enlace[]> {
    return this.enlaceRepository.find();
  }

  findOne(id: number): Promise<Enlace> {
    return this.enlaceRepository.findOneBy({id});
  }

  update(id: number, updateEnlaceDto: UpdateEnlaceDto): Promise<Enlace> {
    const enlaceEntity: Enlace = new Enlace();
    enlaceEntity.id = id;
    enlaceEntity.titulo = updateEnlaceDto.titulo;
    enlaceEntity.url = updateEnlaceDto.url;
    return this.enlaceRepository.save(enlaceEntity);
  }

  remove(id: number): Promise<{affected?: number}> {
    return this.enlaceRepository.delete(id);
  }
}
