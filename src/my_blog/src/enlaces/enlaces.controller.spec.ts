import { Test, TestingModule } from '@nestjs/testing';
import { EnlacesController } from './controller/enlaces.controller';
import { EnlacesService } from './services/impl/enlaces.service.impl';

describe('EnlacesController', () => {
  let controller: EnlacesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EnlacesController],
      providers: [EnlacesService],
    }).compile();

    controller = module.get<EnlacesController>(EnlacesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
