import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EnlacesService } from './services/impl/enlaces.service.impl';
import { EnlacesController } from './controller/enlaces.controller';
import { Enlace } from './entities/enlace.entity';
import { EnlaceBusiness } from './business/impl/enlaces.business.impl';

@Module({

  imports: [TypeOrmModule.forFeature([Enlace])],
  controllers: [EnlacesController],
  providers: [EnlaceBusiness, {provide: 'IEnlacesService', useClass: EnlacesService}],
  
})
export class EnlacesModule {}
