import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';
import { CustomLogger } from './config/CustomLogger';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  //marco de aplicacion web para Node
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  //transforma datos antes de que llegue a los controladores. En este caso se usa para realizar la validacion automatica
  //de datos en toda la aplicacion
  app.useGlobalPipes(new ValidationPipe());
  
  app.useLogger(new CustomLogger());

  const config = new DocumentBuilder()
    .setTitle('Mi blog - validacion datos')
    .setDescription('Validacion datos')
    .setVersion('1.0')
    .addTag('validacion-datos')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3001);
}
bootstrap();
